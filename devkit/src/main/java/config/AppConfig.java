package config;

import helpers.Utils;

public class AppConfig {
    private String xmiFilePath;
    private String outputFolder;
    private String configFolder;

    public String getXmiFilePath() {
        Utils.validateFileExistence(xmiFilePath);
        return xmiFilePath;
    }

    public void setXmiFilePath(String xmiFilePath) {
        Utils.validateFileExistence(xmiFilePath);
        this.xmiFilePath = xmiFilePath;
    }

    public String getOutputFolder() {
        return outputFolder;
    }

    public void setOutputFolder(String outputFolder) {
        this.outputFolder = outputFolder;
    }

    public String getConfigFolder() {
        Utils.validateFolderExistence(configFolder);
        return configFolder;
    }

    public void setConfigFolder(String configFolder) {
        Utils.validateFolderExistence(configFolder);
        this.configFolder = configFolder;
    }
}
