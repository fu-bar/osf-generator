package helpers;

import com.google.common.reflect.ClassPath;
import exceptions.GenException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.stream.Collectors;

public class JarHelper {
    private final Logger log = LogManager.getLogger(JarHelper.class);
    private final Map<String, Class<?>> idToClass = new HashMap<>();

    public Map<String, Class<?>> loadJars(String jarsFolderPath, String basePackage) {
        File dir = new File(jarsFolderPath);
        File[] files = dir.listFiles((folder, filename) -> filename.toLowerCase(Locale.ROOT).endsWith(".jar"));
        List<URL> jarsInFolder = Arrays.stream(files).map(file -> {
            try {
                return file.toURI().toURL();
            } catch (MalformedURLException e) {
                log.error(e.getMessage(), e);
                throw new GenException(e.getMessage(), e);
            }
        }).collect(Collectors.toList());

        URL[] urls = new URL[jarsInFolder.size()];
        jarsInFolder.toArray(urls);
        URLClassLoader classLoader = new URLClassLoader(
                urls,
                JarHelper.class.getClassLoader()
        );

        Set<ClassPath.ClassInfo> classesInPackage;
        try {
            classesInPackage = ClassPath.from(classLoader).getTopLevelClassesRecursive(basePackage);
        } catch (IOException e) {
            this.log.error(e.getMessage(), e);
            throw new GenException(e.getMessage(), e);
        }

        classesInPackage.forEach(classInfo -> idToClass.put(classInfo.getPackageName(), classInfo.load()));

        return idToClass;
    }
}
