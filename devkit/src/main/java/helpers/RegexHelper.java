package helpers;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexHelper {
    private static Map<String, Pattern> stringToPattern = new HashMap<>();

    public static Matcher findMatches(String pattern, String string) {
        if (!stringToPattern.containsKey(pattern)) {
            stringToPattern.put(pattern, Pattern.compile(pattern, Pattern.DOTALL));
        }
        return stringToPattern.get(pattern).matcher(string);
    }
}
