package helpers;

import exceptions.GenException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class Utils {
    private static final Logger log = LogManager.getLogger(Utils.class);

    private Utils() {
        // Static class
    }

    public static void validateFileExistence(String filePath) {
        File file = new File(filePath);
        if (!file.exists() || file.isDirectory()) {
            String errorMessage = String.format("Not a file: %s", filePath);
            log.error(errorMessage);
            throw new GenException(errorMessage);
        }
    }

    public static void validateFolderExistence(String folderPath) {
        File file = new File(folderPath);
        if (!file.exists() || file.isFile()) {
            String errorMessage = String.format("Not a folder: %s", folderPath);
            log.error(errorMessage);
            throw new GenException(errorMessage);
        }
    }
}
