package helpers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import exceptions.GenException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Serialization {
    private static final Logger log = LogManager.getLogger(Serialization.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    private Serialization() {
        // Static class
    }

    public static String toJson(Object object) {
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
            throw new GenException(e.getMessage());
        }
    }

    public static <T> T fromJson(String json, Class<?> targetClass) {
        try {
            return (T) mapper.readValue(json, targetClass);
        } catch (JsonProcessingException e) {
            String errorMessage = String.format("Failed parsing json: %s", json);
            log.error(errorMessage, e);
            throw new GenException(errorMessage, e);
        }
    }
}
