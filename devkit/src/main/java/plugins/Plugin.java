package plugins;

import config.AppConfig;
import exceptions.GenException;
import helpers.Serialization;
import helpers.Utils;
import helpers.XMLHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public abstract class Plugin {
    public final String id = this.getClass().getCanonicalName();
    public final int version;
    public final int minRequiredVersion;
    protected final Logger log = LogManager.getLogger(this.getClass());
    protected final XMLHelper xmlHelper = new XMLHelper();
    private Object config;

    private AppConfig appConfig;

    protected Plugin(int version, int minRequiredVersion) {
        this.version = version;
        this.minRequiredVersion = minRequiredVersion;
    }

    public abstract void process(Document xmiDocument);

    public abstract String getDescription();

    public String getId() {
        return this.id;
    }

    public AppConfig getAppConfig() {
        return appConfig;
    }

    public void setAppConfig(AppConfig appConfig) {
        this.appConfig = appConfig;
    }

    protected <T> T getConfig(Class<?> configClass) {
        if (this.appConfig == null) {
            String errorMessage = "Application configuration is not set";
            log.error(errorMessage);
            throw new GenException(errorMessage);
        }
        File configFolder = new File(this.appConfig.getConfigFolder());
        File expectedConfigFilePath = new File(configFolder, String.format("%s.json", this.getId()));
        Utils.validateFileExistence(expectedConfigFilePath.getPath());
        try {
            return Serialization.fromJson(Files.readString(expectedConfigFilePath.toPath(), StandardCharsets.UTF_8), configClass);
        } catch (IOException e) {
            throw new GenException(e.getMessage());
        }
    }

}
