package osf.generator.plugins.elm;

import exceptions.GenException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnnotationInfo {

    private boolean hasFields;

    private Class<?> annotationClass;

    private Map<String, Object> details = new HashMap<>();

    private List<Class<?>> mentionedClasses = new ArrayList<>();

    public AnnotationInfo(boolean hasFields) {
        this.hasFields = hasFields;
    }

    public void setDetail(String key, Object value) {
        if (value.getClass().isAssignableFrom(Class.class)) {
            mentionedClasses.add((Class<?>) value);
        }
        if (details.containsKey(key)) {
            throw new GenException(String.format("Key %s already exists", key));
        }
        details.put(key, value);
    }

    public Class<?> getAnnotationClass() {
        return annotationClass;
    }

    public void setAnnotationClass(Class<?> annotationClass) {
        this.annotationClass = annotationClass;
    }

    public boolean getHasFields() {
        return hasFields;
    }

    public Map<String, Object> getDetails() {
        return details;
    }

    public List<Class<?>> getMentionedClasses() {
        return mentionedClasses;
    }
}
