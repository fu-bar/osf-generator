package osf.generator.plugins.elm.blocks.enums;

public enum CollectionType {
    NONE,
    LIST,
    SET
}
