package osf.generator.plugins.elm;

import org.w3c.dom.Document;
import osf.generator.plugins.elm.blocks.Generator;
import osf.generator.plugins.elm.blocks.TypeInfo;
import osf.generator.plugins.elm.blocks.XMIParser;
import osf.generator.plugins.elm.config.ELMPluginConfig;
import plugins.Plugin;

import java.util.List;

public class ELMPlugin extends Plugin {
    private static final int PLUGIN_VERSION = 1;
    private static final int MIN_REQUIRED_VERSION = Integer.MIN_VALUE;
    private ELMPluginConfig config;

    public ELMPlugin() {
        super(PLUGIN_VERSION, MIN_REQUIRED_VERSION);
    }

    @Override
    public void process(Document xmiDocument) {
        config = getConfig(ELMPluginConfig.class);
        XMIParser xmiParser = new XMIParser();
        List<TypeInfo> classes = xmiParser.parseXML(getAppConfig().getXmiFilePath(), config);
        log.debug(String.format("Collected information on %d classes", classes.size()));

        Generator generator = new Generator();
        log.debug(String.format("Files will be generated under %s", "c:/temp"));
        classes.forEach(typeInfo -> generator.generate(this.config, typeInfo, "c:/temp", "com.osf.example"));
        log.info("Done :-)");
    }

    @Override
    public String getDescription() {
        return "Generation of ELM entities";
    }
}
