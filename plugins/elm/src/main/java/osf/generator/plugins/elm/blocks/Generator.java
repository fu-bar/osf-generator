package osf.generator.plugins.elm.blocks;


import com.squareup.javapoet.*;
import exceptions.GenException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import osf.generator.plugins.elm.AnnotationInfo;
import osf.generator.plugins.elm.blocks.enums.CollectionType;
import osf.generator.plugins.elm.config.ELMPluginConfig;
import osf.modules.elm.OSFImmutableEntity;

import javax.lang.model.element.Modifier;
import java.io.File;
import java.io.IOException;
import java.util.*;


// TODO: Handle collections
// TODO: Handle indexes
// TODO: Get generated package from XMI
// TODO: Add references constraint annotations
// TODO: Generate additional classes AS-IS (but as read-only)
public class Generator {
    private static final Logger log = LogManager.getLogger(Generator.class);
    private static final String CREATION_TOKEN = "creationToken";
    private TypeResolver typeResolver;

    public void generate(ELMPluginConfig config, TypeInfo typeInfo, String targetFolder, String packageFQN) {
        typeResolver = new TypeResolver(config);
        boolean folderCreated = new File(targetFolder).mkdir();// Create target folder if does not exist
        if (folderCreated) {
            log.info(String.format("Folder created: %s", targetFolder));
        }
        String generatedClassName = typeInfo.getName();

        TypeSpec.Builder immutable = TypeSpec.classBuilder(generatedClassName).addModifiers(Modifier.PUBLIC);
        TypeSpec.Builder writable = TypeSpec.classBuilder(String.format("%sWritable", generatedClassName)).addModifiers(Modifier.PUBLIC);
        List<TypeSpec.Builder> bothClasses = Arrays.asList(immutable, writable);

        if (typeInfo.getInherits().isEmpty()) {
            immutable.superclass(OSFImmutableEntity.class);
        } else {
            immutable.superclass(typeResolver.resolve(typeInfo.getInherits().get()));
        }

        List<FieldInfo> fieldDetails = typeInfo.getFieldInfos();
        List<FieldSpec> privateFields = buildPrivateFields(fieldDetails);

        privateFields.forEach(privateField -> bothClasses.forEach(clazz -> clazz.addField(privateField)));
        bothClasses.forEach(clazz -> clazz.addMethods(buildGetters(privateFields)));
        writable.addMethods(buildSetters(privateFields));
        writable.addMethods(buildWritableConstructors(writable.build(), privateFields, packageFQN));
        immutable.addMethods(buildImmutableConstructors(immutable.build(), privateFields, packageFQN));

        writeToFile(targetFolder, packageFQN, writable.build());
        writeToFile(targetFolder, packageFQN, immutable.build());
    }

    private List<FieldSpec> buildPrivateFields(List<FieldInfo> fieldInfos) {
        List<FieldSpec> privateFields = new ArrayList<>();
        fieldInfos.forEach(fieldInfo -> {
            Class<?> fieldType = typeResolver.resolve(fieldInfo.getFieldType());
            // Handling case of a collection (List/Set)
            FieldSpec.Builder field;
            CollectionType collectionType = fieldInfo.getCollectionType();
            if (collectionType == CollectionType.LIST) {
                field = FieldSpec.builder(ParameterizedTypeName.get(List.class, fieldType), fieldInfo.getName());
            } else if (collectionType == CollectionType.SET) {
                field = FieldSpec.builder(ParameterizedTypeName.get(Set.class, fieldType), fieldInfo.getName());
            } else {
                field = FieldSpec.builder(fieldType, fieldInfo.getName());
            }
            // ----------------------------------------
            field.addModifiers(Modifier.PRIVATE, Modifier.FINAL); // TODO: Consider when to remove the final modifier

            List<AnnotationInfo> annotations = fieldInfo.getFieldAnnotations();
            for (AnnotationInfo annotationInfo : annotations) {
                AnnotationSpec.Builder fieldMetadata = AnnotationSpec.builder(annotationInfo.getAnnotationClass());
                for (Map.Entry<String, Object> detail : annotationInfo.getDetails().entrySet()) {
                    if (String.class.isAssignableFrom(detail.getValue().getClass())) {
                        fieldMetadata.addMember(detail.getKey(), "$S", detail.getValue());
                    } else if (Class.class.isAssignableFrom(detail.getValue().getClass())) {
                        fieldMetadata.addMember(detail.getKey(), "$T", detail.getValue());
                    } else {
                        fieldMetadata.addMember(detail.getKey(), "$L", detail.getValue());
                    }
                }
                field.addAnnotation(fieldMetadata.build());
            }
            privateFields.add(field.build());
        });
        return privateFields;
    }

    private List<MethodSpec> buildGetters(List<FieldSpec> fieldSpecs) {
        List<MethodSpec> getters = new ArrayList<>();
        fieldSpecs.forEach(fieldSpec -> {
            String fieldName = fieldSpec.name;
            String getterName = String.format("get%s", fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1));
            MethodSpec getter = MethodSpec.methodBuilder(getterName)
                    .addModifiers(Modifier.PUBLIC)
                    .returns(fieldSpec.type)
                    .addStatement(String.format("return this.%s", fieldName))
                    .build();
            getters.add(getter);
        });
        return getters;
    }

    private List<MethodSpec> buildSetters(List<FieldSpec> fieldSpecs) {
        List<MethodSpec> getters = new ArrayList<>();
        fieldSpecs.forEach(fieldSpec -> {
            String fieldName = fieldSpec.name;
            String getterName = String.format("set%s", fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1));
            MethodSpec getter = MethodSpec.methodBuilder(getterName)
                    .addModifiers(Modifier.PUBLIC)
                    .returns(void.class)
                    .addParameter(fieldSpec.type, "value")
                    .addStatement(String.format("this.%s = %s", fieldName, "value"))
                    .build();
            getters.add(getter);
        });
        return getters;
    }

    private List<MethodSpec> buildWritableConstructors(TypeSpec typeSpec, List<FieldSpec> fieldSpecs, String packageFQN) {
        List<MethodSpec> constructors = new ArrayList<>();
        MethodSpec defaultConstructor = MethodSpec.methodBuilder(typeSpec.name)
                .addModifiers(Modifier.PUBLIC)
                .addParameter(UUID.class, CREATION_TOKEN)
                .addStatement("super(creationToken)")
                .build();
        constructors.add(defaultConstructor);


        ClassName copyConstructorClassName = ClassName.get("", typeSpec.name);
        MethodSpec.Builder copyConstructor = MethodSpec.methodBuilder(typeSpec.name)
                .addModifiers(Modifier.PUBLIC)
                .addParameter(UUID.class, CREATION_TOKEN)
                .addParameter(copyConstructorClassName, "source")
                .addStatement("super(creationToken)");
        fieldSpecs.forEach(fieldSpec -> copyConstructor.addStatement(setterStatementFor(fieldSpec)));

        constructors.add(copyConstructor.build());

        ClassName copyConstructorFromImmutableClassName = ClassName.get(packageFQN, typeSpec.name.replace("Writable", ""));
        MethodSpec.Builder fromImmutableCopyConstructor = MethodSpec.methodBuilder(typeSpec.name)
                .addModifiers(Modifier.PUBLIC)
                .addParameter(UUID.class, CREATION_TOKEN)
                .addParameter(copyConstructorFromImmutableClassName, "source")
                .addStatement("super(creationToken)");

        fieldSpecs.forEach(fieldSpec -> fromImmutableCopyConstructor.addStatement(setterStatementFor(fieldSpec)));

        constructors.add(fromImmutableCopyConstructor.build());

        return constructors;
    }

    private List<MethodSpec> buildImmutableConstructors(TypeSpec typeSpec, List<FieldSpec> fieldSpecs, String packageFQN) {
        List<MethodSpec> constructors = new ArrayList<>();
        ClassName writableEntityClassName = ClassName.get(packageFQN, String.format("%sWritable", typeSpec.name));
        ClassName cloningUtilsClassName = ClassName.get("osf.objects.utilities", "CloningUtils");

        MethodSpec.Builder fromWritableCopyConstructor = MethodSpec.methodBuilder(typeSpec.name)
                .addModifiers(Modifier.PUBLIC)
                .addParameter(UUID.class, CREATION_TOKEN)
                .addParameter(writableEntityClassName, "source")
                .addStatement("super(creationToken)");
        fieldSpecs.forEach(fieldSpec -> {
            String fieldPart = fieldSpec.name.substring(0, 1).toUpperCase() + fieldSpec.name.substring(1);
            fromWritableCopyConstructor.addStatement(String.format("this.%s = $T.deepClone(source.get%s())", fieldSpec.name, fieldPart), cloningUtilsClassName);
        });
        constructors.add(fromWritableCopyConstructor.build());
        return constructors;
    }

    private void writeToFile(String targetFolder, String packageFQN, TypeSpec typeSpec) {
        JavaFile javaFile = JavaFile.builder(packageFQN, typeSpec).build();
        try {
            javaFile.writeTo(new File(targetFolder));
        } catch (IOException e) {
            String errorMessage = String.format("Failed writing to %s", targetFolder);
            log.error(errorMessage);
            throw new GenException(errorMessage);
        }
    }

    private String setterStatementFor(FieldSpec fieldSpec) {
        String fieldPart = fieldSpec.name.substring(0, 1).toUpperCase() + fieldSpec.name.substring(1);
        return String.format("this.set%s(source.get%s())", fieldPart, fieldPart);
    }

}
