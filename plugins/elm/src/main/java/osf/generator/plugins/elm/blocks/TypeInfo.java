package osf.generator.plugins.elm.blocks;

import org.w3c.dom.Node;
import osf.generator.plugins.elm.AnnotationInfo;
import osf.generator.plugins.elm.blocks.enums.CodeType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TypeInfo {

    private final List<AnnotationInfo> annotations = new ArrayList<>();
    private final List<FieldInfo> fieldInfos = new ArrayList<>();
    private final Node node;
    private String id;
    private String name;
    private CodeType codeType;
    private String inherits;

    public TypeInfo(Node node) {
        this.node = node;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeInfo typeInfo = (TypeInfo) o;
        return com.google.common.base.Objects.equal(id, typeInfo.id);
    }

    @Override
    public int hashCode() {
        return com.google.common.base.Objects.hashCode(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AnnotationInfo> getAnnotations() {
        return annotations;
    }

    public CodeType getCodeType() {
        return codeType;
    }

    public void setCodeType(CodeType codeType) {
        this.codeType = codeType;
    }

    public List<FieldInfo> getFieldInfos() {
        return fieldInfos;
    }

    public Optional<String> getInherits() {
        if (inherits == null) {
            return Optional.empty();
        }
        return Optional.of(inherits);
    }

    public void setInherits(String inherits) {
        this.inherits = inherits;
    }

    public Node getNode() {
        return node;
    }

}
