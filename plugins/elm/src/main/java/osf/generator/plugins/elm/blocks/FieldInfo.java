package osf.generator.plugins.elm.blocks;


import osf.generator.plugins.elm.AnnotationInfo;
import osf.generator.plugins.elm.blocks.enums.CollectionType;

import java.util.ArrayList;
import java.util.List;

public class FieldInfo {

    private final List<AnnotationInfo> annotations = new ArrayList<>();
    private String name;
    private String fieldType;
    private CollectionType collectionType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public List<AnnotationInfo> getFieldAnnotations() {
        return annotations;
    }

    public CollectionType getCollectionType() {
        return collectionType;
    }

    public void setCollectionType(CollectionType collectionType) {
        this.collectionType = collectionType;
    }

    public List<AnnotationInfo> getAnnotations() {
        return annotations;
    }
}
