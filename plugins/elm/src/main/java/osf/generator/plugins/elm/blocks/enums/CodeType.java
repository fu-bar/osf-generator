package osf.generator.plugins.elm.blocks.enums;

public enum CodeType {
    CLASS,
    DATA_TYPE
}
