package osf.generator.plugins.elm.blocks;


import exceptions.GenException;
import helpers.RegexHelper;
import helpers.Serialization;
import helpers.XMLHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import osf.generator.plugins.elm.AnnotationInfo;
import osf.generator.plugins.elm.blocks.enums.CodeType;
import osf.generator.plugins.elm.blocks.enums.CollectionType;
import osf.generator.plugins.elm.config.ELMPluginConfig;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import static code.blocks.constants.Xpath.*;

public class XMIParser {

    private static final String XML_ENCODED_NEWLINE = "&#xA";
    private final Logger log = LogManager.getLogger(XMIParser.class);
    private final XMLHelper xmlHelper = new XMLHelper();
    private final Map<String, TypeInfo> idToInfo = new HashMap<>();
    private ELMPluginConfig config;
    private TypeResolver typeResolver;

    public List<TypeInfo> parseXML(String xmiFilePath, ELMPluginConfig config) {
        this.config = config;
        this.typeResolver = new TypeResolver(config);
        validateFile(xmiFilePath);
        log.info(String.format("Parsing file: %s", xmiFilePath));

        Document document = xmlHelper.readFile(xmiFilePath);
        List<Node> classes = xmlHelper.fetchXpath(document, CLASSES);
        List<Node> dataTypes = xmlHelper.fetchXpath(document, DATA_TYPES);
        log.info(String.format("Collected %d classes and %d data-types", classes.size(), dataTypes.size()));


        List<Node> classExtensions = xmlHelper.fetchXpath(document, CLASS_EXTENSIONS);
        List<Node> dataTypeExtensions = xmlHelper.fetchXpath(document, DATATYPE_EXTENSIONS);
        log.info(String.format("Collected %d class-extensions and %d data-type extensions", classExtensions.size(), dataTypeExtensions.size()));

        return readInfo(classes, dataTypes, classExtensions).stream()
                .filter(typeInfo -> typeInfo.getCodeType().equals(CodeType.CLASS))
                .collect(Collectors.toList());
    }

    private void validateFile(String xmiFilePath) {
        File file = new File(xmiFilePath);
        if (!file.exists() || file.isDirectory()) {
            String errorMessage = String.format("Not a file: %s", xmiFilePath);
            log.error(errorMessage);
            throw new GenException(errorMessage);
        }
    }

    private List<TypeInfo> readInfo(List<Node> classes, List<Node> dataTypes, List<Node> classExtensions) {
        log.debug(String.format("Processing: %d classes, %d data-types, %d class-extensions",
                classes.size(), dataTypes.size(), classExtensions.size()));
        idToInfo.clear();
        readInitialInfo(classes, dataTypes);
        checkForInheritance(classes);
        readFields(classExtensions);
        ArrayList<TypeInfo> baseForCodeGeneration = new ArrayList<>(idToInfo.values());
        log.trace(String.format("Base for code generation: %s", Serialization.toJson(baseForCodeGeneration)));
        return baseForCodeGeneration;
    }

    private void readInitialInfo(List<Node> classes, List<Node> dataTypes) {
        Arrays.asList(classes, dataTypes).forEach(
                collection -> collection.forEach(
                        node -> {
                            TypeInfo typeInfo = new TypeInfo(node);
                            String type = xmlHelper.getAttribute(node, XMI_TYPE_ATTRIBUTE);
                            if (!SUPPORTED_ELEMENTS.contains(type)) {
                                String errorMessage = String.format("Element type is not supported: %s (must be one of: %s)", type, SUPPORTED_ELEMENTS);
                                log.error(errorMessage);
                                throw new GenException(errorMessage);
                            }
                            typeInfo.setCodeType(type.equals(UML_CLASS) ? CodeType.CLASS : CodeType.DATA_TYPE);

                            typeInfo.setName(xmlHelper.getAttribute(node, NAME_ATTRIBUTE));
                            typeInfo.setId(xmlHelper.getAttribute(node, XMI_ID_ATTRIBUTE));

                            // Getting class-annotations

                            validateNodeId(typeInfo.getName(), typeInfo.getId());
                            idToInfo.put(typeInfo.getId(), typeInfo);
                            log.trace(String.format("[BASIC]: %s", Serialization.toJson(typeInfo)));
                        }
                ));
    }

    private void readFields(List<Node> classExtensions) {
        classExtensions.forEach(node -> {

            String referencedId = xmlHelper.getAttribute(node, XMI_ID_REF_ATTRIBUTE);
            TypeInfo typeInfo;
            if (idToInfo.containsKey(referencedId)) {
                typeInfo = idToInfo.get(referencedId);
            } else {
                return;
            }
            List<Node> classAnnotations = xmlHelper.fetchXpath(node, "tags/tag[@name=\"annotations\"]");
            for (Node classAnnotation : classAnnotations) {
                String annotationString = xmlHelper.getAttribute(classAnnotation, "value");
                typeInfo.getAnnotations().add(parseAnnotation(annotationString));
            }
            TypeInfo finalTypeInfo = typeInfo;

            // Fields
            List<Node> attributes = xmlHelper.fetchXpath(node, CLASS_ATTRIBUTES);
            attributes.forEach(attribute -> {
                FieldInfo fieldInfo = new FieldInfo();
                fieldInfo.setName(xmlHelper.getAttribute(attribute, NAME_ATTRIBUTE));
                Node properties = xmlHelper.fetchXpath(attribute, "properties").get(0);
                fieldInfo.setFieldType(xmlHelper.getAttribute(properties, "type"));
                String collection = xmlHelper.getAttribute(properties, "collection");
                if (collection.equals(FALSE)) {
                    fieldInfo.setCollectionType(CollectionType.NONE);
                } else {
                    String duplicates = xmlHelper.getAttribute(properties, "duplicates");
                    fieldInfo.setCollectionType(duplicates.equals(FALSE) ? CollectionType.SET : CollectionType.LIST);
                }

                // TODO: Handle field annotations
                List<Node> fieldAnnotations = xmlHelper.fetchXpath(attribute, "tags/tag[@name=\"annotations\"]");
                for (Node fieldAnnotation : fieldAnnotations) {
                    String annotationsString = xmlHelper.getAttribute(fieldAnnotation, "value");
                    Matcher matches = RegexHelper.findMatches("\\@(.*?)(\\n|$)", annotationsString);
                    int countAnnotations = 0;
                    while (matches.find()) {
                        countAnnotations++;
                        String annotationName = matches.group(1);
                        fieldInfo.getAnnotations().add(parseAnnotation(annotationName));
                    }
                }
                finalTypeInfo.getFieldInfos().add(fieldInfo);
            });
            log.trace(String.format("[WITH-FIELDS]: %s", Serialization.toJson(finalTypeInfo)));
        });
    }

    private AnnotationInfo parseAnnotation(String annotationString) {
        AnnotationInfo annotationInfo = new AnnotationInfo(annotationString.contains("="));
        Matcher matcher = RegexHelper.findMatches("([A-Za-z]*)\\(.*", annotationString);
        if (!matcher.find()) {
            annotationInfo.setAnnotationClass(typeResolver.resolve(annotationString));
            return annotationInfo;
        }
        String name = matcher.group(1);

        Class<?> annotationClass = typeResolver.resolve(name);
        annotationInfo.setAnnotationClass(annotationClass);
        matcher = RegexHelper.findMatches("([A-Za-z0-9]*)\\s*=\\s*(.*?)\\)\\s*$", annotationString);
        while (matcher.find()) {
            String key = matcher.group(1);
            Object value = parseAnnotationValue(matcher.group(2));
            annotationInfo.setDetail(key, value);
        }
        return annotationInfo;
    }

    private Object parseAnnotationValue(String value) {
        if (value.startsWith("\"")) {
            return value.replace("\"", "");
        } else if (value.contains(".class")) {
            return typeResolver.resolve(value.replace(".class", ""));
        } else {
            try {
                return Integer.parseInt(value);
            } catch (NumberFormatException e) {
                try {
                    return Double.parseDouble(value);
                } catch (NumberFormatException ee) {
                    return Boolean.parseBoolean(value);
                }
            }
        }
    }

    private void checkForInheritance(List<Node> classes) {
        log.debug("Checking for inheritance");
        classes.forEach(node -> {
            List<Node> inheritsFrom = xmlHelper.fetchXpath(node, GENERALIZATION_ATTRIBUTE);
            if (!inheritsFrom.isEmpty()) {
                // Assuming a single inheritance (JAVA)
                String currentId = xmlHelper.getAttribute(node, XMI_ID_ATTRIBUTE);
                String parentId = xmlHelper.getAttribute(inheritsFrom.get(0), GENERAL_ATTRIBUTE);
                String parentClassName = idToInfo.get(parentId).getName();
                idToInfo.get(currentId).setInherits(parentClassName);
                log.info(String.format("[Inheritance] %s inherits from %s", idToInfo.get(currentId).getName(), parentClassName));
            }
        });
    }

    private void validateNodeId(String nodeName, String id) {
        if (idToInfo.containsKey(id)) {
            throw new GenException(String.format("Already dealt with id of %s : %s", nodeName, id));
        }
    }
}
