package code.blocks.constants;

import com.google.common.collect.Sets;

import java.util.Set;
import java.util.regex.Pattern;

public class Xpath {
    public static final String CLASSES = "/XMI/Model/packagedElement/packagedElement[@type=\"uml:Class\"]";
    public static final String DATA_TYPES = "/XMI/Model/packagedElement/packagedElement[@type=\"uml:DataType\"]";
    public static final String CLASS_EXTENSIONS = "//Extension/elements/element[@type=\"uml:Class\"]";
    public static final String DATATYPE_EXTENSIONS = "//Extension/elements/element[@type=\"uml:DataType\"]";
    public static final String CLASS_ATTRIBUTES = "attributes/attribute";
    public static final String UML_CLASS = "uml:Class";
    public static final String UML_DATA_TYPE = "uml:DataType";
    public static final String FALSE = "false";
    public static final Pattern STEREOTYPE_REGEX = Pattern.compile("STEREO;Name=(.*?);");

    public static final String NAME_ATTRIBUTE = "name";
    public static final String XMI_TYPE_ATTRIBUTE = "xmi:type";
    public static final String XMI_ID_ATTRIBUTE = "xmi:id";
    public static final String GENERALIZATION_ATTRIBUTE = "generalization";
    public static final String GENERAL_ATTRIBUTE = "general";
    public static final String XMI_ID_REF_ATTRIBUTE = "xmi:idref";


    public static final Set<String> SUPPORTED_ELEMENTS = Sets.newHashSet(UML_CLASS, UML_DATA_TYPE);

    private Xpath() {
        // Static class
    }
}
