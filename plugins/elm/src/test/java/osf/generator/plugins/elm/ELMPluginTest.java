package osf.generator.plugins.elm;

import com.fasterxml.jackson.databind.ObjectMapper;
import config.AppConfig;
import helpers.XMLHelper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

class ELMPluginTest {

    @Test
    void x() throws IOException {
        ELMPlugin elmPlugin = new ELMPlugin();
        ObjectMapper mapper = new ObjectMapper();
        AppConfig appConfig = new AppConfig();
        appConfig.setXmiFilePath("c:\\workspace\\xslt\\xmi\\level2.xml");
        appConfig.setConfigFolder("c:\\temp");
        appConfig.setConfigFolder("C:\\workspace\\osf-generator\\config");
        elmPlugin.setAppConfig(appConfig);
        XMLHelper xmlHelper = new XMLHelper();
        elmPlugin.process(xmlHelper.readFile(appConfig.getXmiFilePath()));
    }
}
