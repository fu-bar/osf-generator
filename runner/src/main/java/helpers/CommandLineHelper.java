package helpers;

import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CommandLineHelper {
    private static final Logger log = LogManager.getLogger(CommandLineHelper.class);

    private CommandLineHelper() {
        // Static class
    }

    public static CommandLine parseCommandLine(String[] args, Options options) {
        Option option = Option.builder("h")
                .longOpt("help")
                .desc("Show this help message")
                .required(false)
                .build();
        options.addOption(option);
        options.addOption(option);
        option = Option.builder("x")
                .longOpt("xmiFilePath")
                .desc("Input XMI (xml) file to be used for code generation")
                .required(true)
                .hasArg(true)
                .build();
        options.addOption(option);
        option = Option.builder("o")
                .longOpt("outputFolder")
                .desc("Folder to hold generation outputs")
                .required()
                .hasArg(true)
                .build();
        options.addOption(option);
        option = Option.builder("c")
                .longOpt("configFolder")
                .desc("Configuration folder")
                .required()
                .hasArg(true)
                .build();
        options.addOption(option);
        option = Option.builder("p")
                .longOpt("pluginsFolder")
                .desc("Folder containing plugin jars")
                .required()
                .hasArg(true)
                .build();
        options.addOption(option);
        CommandLine cmd = null;
        try {
            cmd = (new DefaultParser()).parse(options, args);
        } catch (ParseException e) {
            log.error(e.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -jar Runner-1.0.jar", options);
            System.exit(1);
        }
        return cmd;
    }
}
