package runner;

import config.AppConfig;
import helpers.JarHelper;
import helpers.XMLHelper;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import plugins.Plugin;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static helpers.CommandLineHelper.parseCommandLine;

public class Runner {
    private static final String PLUGINS_BASE_PACKAGE = "osf.generator.plugins";

    public static void main(String[] args) {
        Logger log = LogManager.getLogger(Runner.class);
        Map<String, Plugin> idToPlugin = new HashMap<>();
        Options options = new Options();
        CommandLine cmd = parseCommandLine(args, options);
        String xmiFilePath = cmd.getOptionValue("xmiFilePath");
        String configFolder = cmd.getOptionValue("configFolder");
        String pluginsFolder = cmd.getOptionValue("pluginsFolder");
        String outputFolder = cmd.getOptionValue("outputFolder");
        XMLHelper xmlHelper = new XMLHelper();
        AppConfig appConfig = new AppConfig();
        appConfig.setConfigFolder(configFolder);
        appConfig.setXmiFilePath(xmiFilePath);
        appConfig.setOutputFolder(outputFolder);

        Arrays.asList(xmiFilePath, configFolder, outputFolder, pluginsFolder).forEach(
                param -> {
                    if (param == null) {
                        HelpFormatter formatter = new HelpFormatter();
                        formatter.printHelp("java -jar xmi-codegen.jar", options);
                        System.exit(1);
                    }
                });

        JarHelper jarHelper = new JarHelper();
        Map<String, Class<?>> pkgToClass = jarHelper.loadJars(pluginsFolder, PLUGINS_BASE_PACKAGE);
        List<Class<?>> pluginClasses = pkgToClass.values().stream()
                .filter(Plugin.class::isAssignableFrom)
                .collect(Collectors.toList());
        log.debug(String.format("Collected %d plugins - initializing with default constructors", pluginClasses.size()));

        pluginClasses.forEach(pluginClass -> {
            try {
                idToPlugin.put(pluginClass.getPackageName(), (Plugin) pluginClass.getConstructor().newInstance());
            } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                log.error(e.getMessage(), e);
                System.exit(1);
            }
        });

        log.info(String.format("Loaded %d plugins : %s", idToPlugin.keySet().size(), idToPlugin.keySet()));
        idToPlugin.forEach((pluginId, plugin) -> {
            log.info(String.format("Executing %s (%s)", pluginId, plugin.getDescription()));
            plugin.setAppConfig(appConfig);
            plugin.process(xmlHelper.readFile(appConfig.getXmiFilePath()));
        });
    }
}
